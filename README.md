# 比较docx文档之间的文本相似度

## 开发环境
TDM-GCC-64(Windows) or g++(Linux)    

## C++标准
c++11  

## 编译  
cd 项目路径     
### Windows    
    mingw32-make    
### Linux  
    make

## 说明
此项目用于数据结构课设，尚在完善中。
