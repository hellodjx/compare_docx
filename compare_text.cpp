#include "compare_text.h"
#include <iostream>

bool word_segment_(const std::string& substr)
{
	return true;
}

float compare_text(const std::string& ref1, const std::string& ref2)
{
	// std::map<std::string, std::pair<size_t, size_t>> container;
	avl_tree<std::string, my::pair<size_t, size_t>> container;

	for(size_t i = 0, start = 0; i < ref1.length(); ++i)
	{
		std::string substr = ref1.substr(start, i - start + 1);
		if(word_segment_(substr))
		{
			++container[substr].first;
			start = i + 1;
		}
	}

	for(size_t i = 0, start = 0; i < ref2.length(); ++i)
	{
		std::string substr = ref2.substr(start, i - start + 1);
		if(word_segment_(substr))
		{
			++container[substr].second;
			start = i + 1;
		}
	}

	unsigned long product = 0;
	unsigned long modulo1 = 0;
	unsigned long modulo2 = 0;

	// for(std::map<std::string, std::pair<size_t, size_t>>::const_iterator it = container.begin(); it != container.end(); ++it)
	// {
	// 	const std::pair<size_t, size_t>& cnt = it->second;
	// 	product += cnt.first * cnt.second;
	// 	modulo1 += cnt.first * cnt.first;
	// 	modulo2 += cnt.second * cnt.second;
	// }
	for(auto it = container.begin(); it != container.end(); ++it)
	{
		const my::pair<size_t, size_t>& cnt = it->second;
		product += cnt.first * cnt.second;
		modulo1 += cnt.first * cnt.first;
		modulo2 += cnt.second * cnt.second;
	}

	return product / (std::sqrt(static_cast<float>(modulo1)) * std::sqrt(static_cast<float>(modulo2)));
}

