#ifndef __COMPARE_TEXT_H__
#define __COMPARE_TEXT_H__

#include <string>
#include <map>
#include <cmath>
#include "avlmap.h"
#include "my_pair.h"

float compare_text(const std::string& ref1, const std::string& ref2);

#endif