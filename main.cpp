#include "zip.h"
#include "transform_encode.h"
#include "tinyxml2.h"
#include "compare_text.h"
#include <cstdlib>
#include <iostream>

using namespace std;


string read_docx(string docx_path)
{
    void *buf = NULL;
    size_t bufsize;

    zip_t *zip = zip_open(docx_path.c_str(), ZIP_DEFAULT_COMPRESSION_LEVEL, 'r');
    if (zip == NULL){
        cout << "File is not exist" << endl;
        throw "File is not exist";
    }
    
    int result = zip_entry_open(zip, "word/document.xml");
    if (result != 0){
        cout << "Can not open " << docx_path << endl;
        throw "can not open file";
    }
    zip_entry_read(zip, &buf, &bufsize);
    
    zip_entry_close(zip);
    zip_close(zip);

    tinyxml2::XMLDocument xml;
	auto res = xml.Parse((char *)buf, bufsize);
    if (res != tinyxml2::XML_SUCCESS){
        cout << "Can not parse file." << endl;
        throw "Can not parse file.";
    }

    auto first_element = xml.FirstChildElement("w:document")->FirstChildElement("w:body")->
    FirstChildElement("w:p")->FirstChildElement("w:r");
    string doc_text;

    if (first_element != 0){
        doc_text = first_element->FirstChildElement("w:t")->GetText();
    }
    else{
        cout << "The doc in " << docx_path << " don't have any text." << endl;
        return "";
    }

    auto element = xml.FirstChildElement("w:document")->FirstChildElement("w:body")->
    FirstChildElement("w:p");
    
    do {
        element = element->NextSiblingElement("w:p");
        if (element == 0){
            break;
        }
        doc_text = doc_text + "\n" + element->FirstChildElement("w:r")->FirstChildElement("w:t")->GetText();
    } while (1);

    return doc_text;
}

int main(int argc, char *argv[])
{   
    string doc1_path;
    string doc2_path;
    string doc1_text;
    string doc2_text;

    cout << get_ture_output_string("请输入docx文档1的路径：");
    cin >> doc1_path;
    cout << get_ture_output_string("请输入docx文档2的路径：");
    cin >> doc2_path;

    try
    {
        doc1_text = read_docx(doc1_path);
        doc2_text = read_docx(doc2_path);
    }
    catch(const exception& e)
    {
        cout << e.what() << '\n';
        return -1;
    }
    
    cout << "text in doc1: " << endl << get_ture_output_string(doc1_text) << endl;
    cout << endl;
    cout << "text in doc2: " << endl << get_ture_output_string(doc2_text) << endl;

    cout << endl;
    cout << get_ture_output_string("文本相似度对比: ") << compare_text(doc1_text, doc2_text) << endl;

    system("pause");

    return 0;
}